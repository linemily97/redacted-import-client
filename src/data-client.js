const { readFileSync } = require("fs");
const { isFalsy, isTruthy } = require("./utils");
const path = require("path");

const load = (filepath) => {
  return JSON.parse(readFileSync(path.resolve(filepath)).toString());
};

/**
 * Initial export of buildings
 * datacenters should have an array of buildings
 * @type {any}
 */
const buildings = load("./src/data/buildings.json");

/**
 * Initial export of datacenters
 * note: some records do not have correct date formats, or buildings associated
 * @type [{id: string, externalId: string, address: string, country: string, city: string, state: string, zipcode: string, owner: string, contactName: string, contactEmail: string, buildingIds: array, buildings: array, import: string, operationalDate: string}]
 */
const dataCenters = load("./src/data/datacenters.json");

/**
 * Provides an interface to datacenter and building data.
 * Also a reasonable place for data mutations/transforms. See `enableImport`;
 */
class DataClient {
  constructor(obj) {
    const { buildings, dataCenters } = obj;

    this.buildings = buildings;
    this.dataCenters = dataCenters;
  }

  /**
   * Datacenters without an id, and therefore insertable
   * @returns {*}
   */
  insertable() {
    return this.dataCenters.filter(({ id }) => isFalsy(id));
  }

  /**
   * Datacenters with an id, and therefore insertable
   * @returns {*}
   */
  updatable() {
    return this.dataCenters.filter(({ id }) => isTruthy(id));
  }

  /**
   * Exported records came with an import field set to false. To process
   * on the api this value must be true.
   */
  enableImport() {
    this.dataCenters = this.dataCenters.map((dc) => ({
      ...dc,
      import: true,

      // change date to ISO String
      operationalDate: new Date(dc.operationalDate).toISOString(),

      // finds the building object in the buildings.json data that corresponds to each of the buildingIds
      buildings: dc.buildingIds.map((id) => {
        return this.buildings.find((building) => building.id === id);
      }),
    }));
    // buildings property must be provided as an array of building objects

    // console.log("DEBUG");
    // console.log(this.buildings[0])
    // for (let i = 0; i < this.dataCenters.length; i++) {
    // this.dataCenters[i].buildingIds.map(id=>{
    // console.log(id)
    // console.log(this.buildings.find(building => building.id == id))
    // })
    // console.log(this.dataCenters[i].buildings)
    // console.log(this.dataCenters[i].buildingIds);
    // }
    // console.log("END DEBUG")
  }
}

const dataClient = new DataClient({ buildings, dataCenters });

module.exports = { dataClient };

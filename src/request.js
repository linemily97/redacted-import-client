const { isFalsy, isJson } = require("./utils");
const defaultOptions = {
  suppressErrors: true,
  logResponse: false,
};

// keeps track of how many total are sent to use in setTimeout so it'll with pause between each
// however global variables are discouraged and bad practice....
global.totalSent = 0;

/**
 * Performs supplied api action.
 */
class Request {
  /**
   *
   * @param {Object} datacenter - `dataCenter` object from dataClient
   * @param {Function} apiAction - apiClient action to take (create, update, etc)
   * @param {Object} options
   * @param {Boolean} options.suppressErrors - If true, will prevent try/catch in perform from throwing error, reporting it back in a pseudo response
   * @param {Boolean} options.logResponse - Will console log response data. Could get messy!
   */
  constructor(datacenter, apiAction = () => {}, options = defaultOptions) {
    this.datacenter = datacenter;
    this.apiAction = apiAction;
    this.options = options;
  }

  get requestArgs() {
    const {
      apiAction: { length: argCount },
      datacenter: dc,
    } = this;
    let args;
    if (argCount === 1) {
      args = [dc];
    } else if (argCount === 2) {
      const { id } = dc;
      if (isFalsy(id)) throw new Error("Must have id to update.");
      args = [id, dc];
    } else {
      throw new Error("weird arg count for fn.");
    }
    return args;
  }

  /**
   * Does the work.
   * Returns a combination of response, and parsed response body.
   * @returns {Promise<({headers: string, error: boolean, body: string, status: string}|{message: *}|string)[]>}
   */

  async perform() {
    const { apiAction } = this;
    let response;
    let body;

    // console.log("DEBUG")
    // Rate limit exceeded. 10 requests per 5000ms
    // await new Promise((resolve) => setTimeout(resolve, 5000));
    // console.log(this.requestArgs)
    // console.log("END DEBUG")
    // return;

    // At first I tried sending 10 at a time and then taking a pause 5000ms..
    // but ran into issues... so I changed it to send 1 at a time with a smaller pause in between each
    // for (let i = 0; i < this.requestArgs.length; i += 10) {

    global.totalSent += 1;

    let promise = new Promise((resolve, reject) => {
      // set timeout to space out the calls
      setTimeout(async () => {
        try {
          response = await apiAction(...this.requestArgs);

          // console.log("DEBUG");
          // console.log(response[0]);
          // console.log("END DEBUG");

          // console.log(response),
          // await new Promise((resolve) => setTimeout(resolve, 5000)),

          if (isJson(response)) {
            body = await response.json();
            resolve();
            // console.log(body);
          } else {
            // 429, 404, and several other responses come back as text.

            // console.log(response)
            // return

            const message = await response.text();
            // console.log("MESSAGE " + message);

            body = { message, error: true, status: response.status };
            resolve();
          }
        } catch (e) {
          // console.log(e)
          if (!this.options.suppressErrors) {
            throw e;
          }
          response = {
            error: true,
            status: "request-failure",
            body: "",
            headers: "",
          };
          body = `${e}\n${e.stackTrace}`;
          reject();
        }
        if (this.options.logResponse) {
          console.log(
            `[${new Date().toTimeString()}] - ${
              this.apiAction.name
            } - response and body:`,
            response,
            body
          );
        }

        //didn't wrap the return at first in the setTimeout... led to many errors and would return undefined before setTimetout finished (oops)
        //   }, 500*global.totalSent)
        // }

        // console.log([response, body])
        // return [response, body];

        // I tried to use 500ms at first, but was still getting a few 429's
      }, 750 * global.totalSent);
    });
    await promise
      .then(() => {
        // console.log("DEBUG")

        // console.log the response so I can see if I'm still getting any 429's while they return... so I don't have to wait for all the records to laod
        console.log([response, body]);
        return [response, body];
      })
      .catch((err) => console.log(err));
  }

  static async perform(
    datacenter,
    apiAction = () => {},
    options = defaultOptions
  ) {
    const request = new this(datacenter, apiAction, options);
    return request.perform();
  }
}

module.exports = { Request };

// 503
// error: 'Production of the modial    error: 'Production of the modial interaction of magneto reluctance, and capacitive duractance has been borked. [Server Error, retry the request.]' interaction of magneto reluctance, and capacitive duractance has been borked. [Server Error, retry the request.]'
//
// 500
// error: 'Attempted read on Write-Only memory. Science broke. [Server Error, retry the request.]'0
